<? include 'layout/header.php'; ?>

<a href="#">Back to Subcontractors List</a>

<form class="mega-form">
    <div class="row-fluid form_row">
        <div class="row-fluid">
            <h3>Sub Contractor Details</h3>
            <div class="span6 form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="company">Company: </label>
                    <div class="controls">
                        <input type="text" name="" id="company" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="adress">Address: </label>
                    <div class="controls">
                        <input type="text" name="" id="adress" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="address">City: </label>
                    <div class="controls">
                        <input type="text" class="input-small" name="" id="city" />
                        <label for="address">State: </label>
                        <select class="input-mini" id="state">
                            <option>State</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="zip">ZIP: </label>
                    <div class="controls">
                        <input type="text" name="" id="zip" class="input-small" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="phone">Phone: </label>
                    <div class="controls">
                        <input type="text" name="" id="phone" class="input-small" />
                    </div>
                </div>
            </div>



            <div class="span6 form-horizontal">

                <div class="control-group">
                    <label class="control-label" for="primary_contact">Primary Contact: </label>
                    <div class="controls">
                        <input type="text" name="" id="primary_contact" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="primary_contact_phone">Phone: </label>
                    <div class="controls">
                        <input type="text" name="" id="primary_contact_phone" />
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="primary_contact_fax">Contract Price: </label>
                    <div class="controls">
                        <input type="text" name="" id="primary_contact_fax" />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid form_row">
        <div class="span6 form-horizontal">
            <h3>General Liability Details</h3>
            <div class="control-group">
                <label class="control-label" for="company_insurance_company">Company: </label>
                <div class="controls">
                    <input type="text" name="" id="insurance_company"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="company_insurance_policy">Policy#: </label>
                <div class="controls">
                    <input type="text" name="" id="insurance_policy"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="company_insurance_covered">Covered: </label>
                <div class="controls">
                    <input type="text" name="" id="insurance_covered"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="company_insurance_attachments">Attachments: </label>
                <div id="insurance_attachments">
                    <!--Attachments like in Phoenix -->
                </div>
            </div>
        </div>

        <div class="span6 form-horizontal">

            <h3>General Liability Details</h3>
            <div class="control-group">
                <label class="control-label" for="workmans_insurance_company">Company: </label>
                <div class="controls">
                    <input type="text" name="" id="insurance_company"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="workmans_insurance_policy">Policy#: </label>
                <div class="controls">
                    <input type="text" name="" id="insurance_policy"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="workmans_insurance_covered">Covered: </label>
                <div class="controls">
                    <input type="text" name="" id="insurance_covered"/>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="workmans_insurance_attachments">Attachments: </label>
                <div id="insurance_attachments">
                    <!--Attachments like in Phoenix -->
                </div>
            </div>

            <button class="btn btn-success" id="submit_job">Save and Continue</button>
            <button class="btn btn-danger" id="cancel_job">Cancel</button>

        </div>
    </div>
</form>


<a href="#">Back to Subcontractors List</a>

<? include 'layout/footer.php'; ?>