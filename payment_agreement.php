<? include 'layout/header.php'; ?>

<div class="row-fluid">
    <div class="span12 text-left">
        <a href="#">Return to PO Overview</a>
    </div>
</div>
<form>
    <? include 'po_header.php'; ?>

    <div class="row-fluid">
        <div style="max-height: 500px; overflow-y: auto">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>PAYMENT</th>
                        <th>DATE TO BE PAID</th>
                        <th>NOTES</th>
                        <th>AMOUNT</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="add_pa_row" data-row="1">
                            <i class="icon-plus"></i>
                        </td>
                        <td colspan="9"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</form>


<? include 'layout/footer.php'; ?>
