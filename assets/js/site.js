var refreshPageAfterModalCloses = false;

/*
 * Diverse Alert Modal takes 1 mandatory parameter 
 * @var body = content to be displayed in the body of the modal.
 * 
 * @var options = object:
 *      title = overwrite with custom modal header,
 *      closeModalText = overwrites the close button with your text,
 *      button = appends a custom button with options{
 *          text = button text,
 *          class = button class,
 *          prepend = false (set to true to prepend button vs append)
 *          href = create onClick hard link,
 *          callback = calls this function when clicked. href takes priority if both are set.
 *          callbackWithParams = same as callback but for functions that receive parameters. Must be sent as a string
 *                               Ex.: callbackWithParams: "delete(1, 'cohort')",
 *          
 *      }
 * @author mikel moreno <mmoreno@certatim.com>
 */
function alert_modal(body, options) {
    //set Defaults
    $('#alert_modal .modal-body').html(body);
    $('#alert_modal .modal-header').html('<h4>System Message</h4>');

    //clear footer of any previous buttons

    $('#alert_modal .modal-footer').html('');
    //re-insert base close button
    $('#alert_modal .modal-footer').html('<button class="btn btn-inverse closeModal">Close</button>');

    //if options parameters are set, instantiate options
    if (options) {

        if (options.width) {
            $('#alert_modal').width(options.width);
            //$('#alert_modal').css("margin-left", (options.width / -2) + "px");
        }

        if (options.height) {
            $('#alert_modal').height(options.height);
        }

        if (options.title) {
            $('#alert_modal .modal-header').html('<h5>' + options.title + '</h5>');
        }

        if (options.closeModalText) {
            $('#alert_modal .modal-footer').html('<button class="btn btn-inverse closeModal">' + options.closeModalText + '</button>');
        }

        if (options.button) {
            $.each(options.button, function(i) {
                if (this.noshow) {
                    $('#alert_modal .modal-footer').hide();
                } else if (this.text) {
                    var btnClass = '';
                    var btnHref = '';

                    if (this.class) {
                        var btnClass = this.class;
                    }
                    if (this.href) {
                        var btnHref = 'onclick="window.location.href=\'' + this.href + '\'"';
                    }
                    var btnText = this.text;

                    //build button based on options
                    var button = '<button class="btn btn-primary ' + btnClass + ' modal_callback" ' + btnHref + '>' + btnText + '</button>';

                    if (this.prepend == true) {
                        $('#alert_modal .modal-footer').prepend(button);
                    } else {
                        $('#alert_modal .modal-footer').append(button);
                    }

                    if (this.callback && !this.href) {
                        $('button.modal_callback').click(function(e) {
                            e.preventDefault();
                            options.button[i].callback();
                        });
                    }
                    if (this.callbackWithParams && !this.href) {
                        $('button.modal_callback').click(function(e) {
                            e.preventDefault();
                            eval(options.button[i].callbackWithParams);
                        });
                    }
                }
            });
        }
    }


    $('#alert_modal').modal('show', {
        backdrop: 'static'
    });


    $('.closeModal').click(function(e) {
        e.preventDefault();
        $(this).parents('div.modal').modal('hide');

        if (refreshPageAfterModalCloses === true)
        {
            location.reload(true);
        }
    });

}

function closeModal() {
    $('#alert_modal').modal('hide');
}

function saveModalForm() {
    $('div.modal-body > form').submit();
}

$('.copy_info input[type="checkbox"]').on('change', function() {
    var input_original = $(this).data('input_original').split(',');
    var input_destination = $(this).data('input_destination').split(',');
    duplicate_values(input_original, input_destination, $(this).is(':checked'));
});

$('.add_value_select').on('click', function() {
    $.get(
            '/modal/new_job_group.php',
            function(data) {
                alert_modal(
                        $(data),
                        {
                            title: 'New Neighborhood',
                            closeModalText: 'Close',
                            button: [{
                                    text: 'OK',
                                    class: 'btn btn-primary',
                                    prepend: true,
                                    callback: saveModalForm
                                }]
                        }
                );
            }
    );

});

$('.child_rows_open i').on('click', function() {
    var row = $(this).parent().data('row');
    $('tr[data-row="row_' + row + '"], .child_rows').toggle();
});

$('.add_po_row').on('click', function() {
    add_row(this, '/modal/po_row.php', {row_number: 1});
});

$('.add_pa_row').on('click', function() {
    add_row(this, '/modal/pa_row.php', {});
});

$('#new_bid').on('click', function() {
    $.get(
            '/modal/new_bid.php',
            function(data) {
                alert_modal(
                        $(data),
                        {
                            title: 'Create New Bid',
                            closeModalText: 'CANCEL',
                            button: [{
                                    text: 'SUBMIT',
                                    class: 'btn btn-primary',
                                    prepend: true,
                                    callback: saveModalForm
                                }]
                        }

                );
                $('.cost_code').on('click', 'i.add_cost_code', function() {
                    var newrow = $(this).parent().clone(true);
                    newrow.find('.remove_cost_code').removeClass('hide');
                    newrow.insertAfter($(this).parent());
                });
                $('.cost_code').on('click', 'i.remove_cost_code', function() {
                    var newrow = $(this).parent().remove();
                });
                $('#add').on('click', function() {
                    $('#subcontractor_list > option:selected').clone().appendTo($('#selected_subcontractors'));
                    $('#subcontractor_list > option:selected').remove();
                });
                $('#remove').on('click', function() {
                    $('#selected_subcontractors > option:selected').clone().appendTo($('#subcontractor_list'));
                    $('#selected_subcontractors > option:selected').remove();
                });
            }
    );
});

function open_cost_code_modal(regime) {
    $.get(
            '/modal/create_cost_code.php?r=' + regime,
            function(data) {
                $(data).appendTo('body');
                $('#cost_code_modal').modal('show', {
                    backdrop: false
                });
            }
    );
}

$('#level1').on('change', function() {
    //Ajax request
    $('#prefix2, #prefix3').html();
    $('#prefix2').html($(this).val());
});

$('#level2').on('change', function() {
    //Ajax request
    $('#prefix2, #prefix3').html();
    $('#prefix3').html($(this).val());
});

$('.cost_code').on('click', function() {
    $(this).parent().siblings('.estimate, .variance').children('input').toggle();
});

$('.delete_cc').on('click', function() {
    alert_modal(
            'Do you permanently want to delete ' + $(this).data('name') + ' from the Cost Code List?',
            {
                title: 'Delete Cost Code',
                closeModalText: 'NO',
                button: [{
                        text: 'YES',
                        class: 'btn btn-primary',
                        prepend: true,
                        callbackWithParams: 'delete_cost_code("' + $(this).data('id') + '")'
                    }]
            }
    );
});

$('.cost_codes  ul  li div').on('click', function() {
    $(this).siblings('ul').toggle();
    $(this).find('i').toggleClass('icon-chevron-down icon-chevron-right');
});

$('#project_start, #project_completion, #actual_start, #actual_completion, .due_date').datepicker();

function delete_cost_code(id) {
    //do Ajax Magic here
    $('[data-id=' + id + ']').parent().remove();
    closeModal();
}

function duplicate_values(input_original, input_destination, remove_dest_values) {
    var length = input_original.length;
    for (var i = 0; i < length; i++) {
        $('#' + input_destination[i]).val(remove_dest_values ? $('#' + input_original[i]).val() : '');
    }
}
function add_row(obj, url, params) {
    var button = $(obj).parent();
    $.post(
            url,
            params,
            function(data) {
                $(data).insertBefore(button);
            }
    );

}