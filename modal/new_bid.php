<form>
    <div class="row-fluid">
        <div class="span12">
            <span>Select Subcontractor(s):</span>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span5">
            <label for="subcontractor_list">Subcontractor List:</label>
            <select id="subcontractor_list" multiple="multiple">
            </select>
            <a class="select_all" href="#">Select All</a>
        </div>
        <div class="span2">
            <div id="arrows">
                <span id="add">Add</span>
                <span id="remove">Remove</span>
            </div>
        </div>
        <div class="span5">
            <label for="selected_subcontractors">Selected Subcontractors:</label>
            <select id="selected_subcontractors" multiple="multiple">
            </select>
            <a class="reset" href="#">Reset</a>
        </div>
    </div>
    <div class="row-fluid form-horizontal">
        <label class="control-label">Cost Code:</label>
        <div class="controls">
            <div class="cost_code">
                <select id="job_group" name="[]"  >
                    <option>Select...</option>
                </select>
                <i class="icon-plus add_cost_code" data-add_value="cost_code"></i>
                <i class="icon-remove remove_cost_code hide"></i>
            </div>
            <span class="btn-link" onclick="open_cost_code_modal(1);">Add additional cost code</span>
        </div>
    </div>
    <div class="row-fluid form-horizontal">
        <label class="control-label">Notes:</label>
        <div class="controls">
            <textarea name="bid_notes">
            </textarea>
        </div>
    </div>
    <div class="row-fluid">
    </div>
</form>