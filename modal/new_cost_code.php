<div id="cost_code_modal" class="hide active fade in modal">
    <div class="modal-header">
        <h5>Create New Cost Code</h5>
    </div>
    <div class="modal-body">
        <form class="form-horizontal new_cc">
            <label class="control-label cc_level_1">Cost Code Level 1:</label>
            <div class="controls cc_level_1">
                <select id="level1" onchange="change_prefix()">
                    <option value="1">1000-1999</option>
                    <option value="2">2000-2999</option>
                    <option value="3">3000-3999</option>
                </select>
                <i class="icon-plus" data-level="1"></i>
            </div>
            <label class="control-label cc_level_2">Cost Code Level 2:</label>
            <div class="controls cc_level_2">
                <span class="prefix" id="prefix1"></span> - 
                <!-- Ajax Majic Here -->
                <select id="level2" onchange="change_prefix()">
                    <option value="1">100-199</option>
                    <option value="2">200-299</option>
                    <option value="3">300-399</option>
                </select>
                <i class="icon-plus" data-level="2"></i>
            </div>
            <label class="control-label cc_level_3">Cost Code Level 3:</label>
            <div class="controls cc_level_3">
                <span class="prefix" id="prefix2"></span> - 
                <!-- And here -->
                <select>
                    <option value="1">10-19</option>
                    <option value="2">20-29</option>
                    <option value="3">30-39</option>
                </select>
                <i class="icon-plus" data-level="3"></i>
            </div>
            <label class="control-label cc_level_4">Cost Code Level 4:</label>
            <div class="controls cc_level_4">
                <input type="text" class="input-mini" name=""> - <input type="text" class="input-medium" name="">
            </div>
            <a href="#">Reset All</a>
        </form>
    </div>
    <div class="modal-footer cc_modal_footer">
        <button class="btn btn-primary" onclick="cost_code_modal('submit');">SUBMIT</button>
        <button class="btn btn-inverse" onclick="cost_code_modal('close');">CANCEL</button>
    </div>
    <script>
                    function cost_code_modal(action) {
                        if (action === 'submit') {
                            $('#cost_code_modal form').submit();
                        } else {
                            $('#cost_code_modal').modal('hide');
                        }
                        $('#cost_code_modal').remove();
                    }
                    function change_prefix() {
                        $('#prefix1').html($('#level1').val());
                        $('#prefix2').html($('#level1').val() + $('#level2').val());
                    }
                    $(document).ready(function() {
                        change_prefix();
                    });
                    $('.new_cc i').on('click', function() {
                        new_cc($(this).data('level'));
                    });

                    function new_cc(level) {
                        $('.new_cc label').toggle();
                        $('div.cc_level_4, i, a').remove();
                        $('.cc_modal_footer > button.btn-primary').attr('onclick', 'new_cc_create(' + level + ');');
                        if (level === 1) {
                            
                            $('label.cc_level_1').toggle();
                            $('div.cc_level_1').html('<input type="text" name="" /><i class="icon-plus" data-level="1"></i>');
                            $('div.cc_level_2, div.cc_level_3, i').remove();
                        } else if (level === 2) {
                            $('label.cc_level_1, label.cc_level_2').toggle();
                            $('div.cc_level_2').html('<input type="text" name="" />');
                            $('div.cc_level_3').remove();
                        } else if (level === 3) {
                            $('label.cc_level_1, label.cc_level_2, label.cc_level_3').toggle();
                            $('div.cc_level_3').html('<input type="text" name="" />');
                        } else {
                            alert('Something went wrong =(')
                        }
                    }
                    
                    function new_cc_create(level){
                        
                        console.log(level);
                    }
    </script>
</div>