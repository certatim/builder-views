<div id="cost_code_modal" class="hide active fade in modal">
    <div class="modal-header">
        <h5>Create New Cost Code</h5>
    </div>
    <div class="modal-body">
        <form class="form-horizontal new_cc">
            <div class="cc_1">
                <label class="control-label cc_level_1">Cost Code Level 1:</label>
                <div class="controls cc_level_1">
                    <select id="level1" onchange="change_prefix()">
                        <option value="1">1000-1999</option>
                        <option value="2">2000-2999</option>
                        <option value="3">3000-3999</option>
                    </select>
                    <input type="text" name="" class="hide" />
                    <i class="icon-plus" data-level="1"></i>
                </div>
            </div>
            <div class="cc_2">
                <label class="control-label cc_level_2">Cost Code Level 2:</label>
                <div class="controls cc_level_2">
                    <span class="prefix" id="prefix1"></span><span class="prefix"> - </span>
                    <!-- Ajax Majic Here -->
                    <select id="level2" onchange="change_prefix()">
                        <option value="1">100-199</option>
                        <option value="2">200-299</option>
                        <option value="3">300-399</option>
                    </select>
                    <input type="text" name="" class="hide" />
                    <i class="icon-plus" data-level="2"></i>
                </div>
            </div>
            <div class="cc_3">
                <label class="control-label cc_level_3">Cost Code Level 3:</label>
                <div class="controls cc_level_3">
                    <span class="prefix" id="prefix2"></span><span class="prefix"> - </span>
                    <!-- And here -->
                    <select>
                        <option value="1">10-19</option>
                        <option value="2">20-29</option>
                        <option value="3">30-39</option>
                    </select>
                    <input type="text" name="" class="hide" />
                    <i class="icon-plus" data-level="3"></i>
                </div>
            </div>
            <div class="cc_4">
                <label class="control-label cc_level_4">Cost Code Level 4:</label>
                <div class="controls cc_level_4">
                    <input type="text" class="input-mini" name="" /><span> - </span><input type="text" class="input-medium" name="" />
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer cc_modal_footer">
        <button class="btn btn-link pull-left" onclick="cost_code_modal('reset');">Reset all</button>
        <button class="btn btn-primary" onclick="cost_code_modal('submit');">SUBMIT</button>
        <button class="btn btn-inverse" onclick="cost_code_modal('close');">CANCEL</button>
    </div>
    <script>

                        function cost_code_modal(action) {
                            if (action === 'reset') {
                                change_row(1);
                            }
                            else if (action === 'submit') {
                                $('#cost_code_modal form').submit();
                            } else if (action === 'close') {
                                $('#cost_code_modal').modal('hide');
                                $('#cost_code_modal').remove();
                            }
                        }

                        function change_prefix() {
                            $('#prefix1').html($('#level1').val());
                            $('#prefix2').html($('#level1').val() + $('#level2').val());
                        }
                        $(document).ready(function() {
                            change_prefix();
                        });


                        $('.new_cc i').on('click', function() {
                            change_row($(this).data('level'));
                        });

                        function change_row(level) {
                            remove_select(level);
                            switch (level) {
                                case 1:
                                    //do ajax generation here for the first row
                                    $('div.cc_level_1').prepend('<span>5000 - 5999</span>');
                                    console.log(level);
                                    break;
                                case 2:
                                    //do ajax generation here for the second row. Optionally could be combined with the second row
                                    $('div.cc_level_2').prepend('<span>5100 - </span>');
                                    console.log(level);
                                    break;
                                case 3:
                                    console.log(level);
                                    break;
                                case 4:
                                    console.log(level);
                                    break;
                                default:
                                    alert('Houston, something went wrong!');
                            }
                            for (var i = level; i < 4; i++) {
                                $('div.cc_' + (i + 1)).addClass('hide');
                            }
                        }
                        function remove_select(level) {
                            add_level_button(level);
                            $('div.cc_level_' + level + ' span.prefix, div.cc_level_' + level + ' i, div.cc_' + level + ' select').remove();
                            $('div.cc_' + level + ' input').removeClass('hide');
                        }
                        function add_level_button(level) {
                            $('.add_level').remove();
                            $('<i class="icon-plus add_level" data-level="' + (level + 1) + '"></i>').appendTo($('.new_cc'));
                            $('i.add_level').on('click', function() {
                                level = $(this).data('level');
                                add_level(level);
                                $(this).data('level', level + 1);
                            });
                        }
                        function add_level(level) {
                            $('div.cc_' + level).removeClass('hide');
                            if (level < 4) {
                                remove_select(level);
                            } else {
                                $('.add_level').remove();
                            }

                        }
                        function create_cc(level) {

                        }
                        function create_select(level, id) {

                        }
    </script>
</div>