<? include 'layout/header.php'; ?>

<h3>Outstanding PO's</h3>
<div class="row-fluid">
    <div style="max-height: 1000px; overflow-y: auto">
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Jobname</th>
                    <th>PO#</th>
                    <th>Title</th>
                    <th>Subcontractor</th>
                    <th>Paid</th>
                    <th>Cost</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="4" >
                        <span class="pull-right">
                            TOTAL LISTED:
                        </span>
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td> 
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>                        
                </tr>
            </tbody>
        </table>
    </div>
</div>



<? include 'layout/footer.php'; ?>


