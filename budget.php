<? include 'layout/header.php'; ?>

<div class="row-fluid">
    <div class="span6 text-left">
        <h2>Job Name</h2>
    </div>
    <div class="span6 text-right">
        <p>Total Budget: $</p>
        <p>Spend-To-Date: $</p>
        <p>Outstanding PO's: $</p>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs">
            <li><a href="/budget.php">Budget</a></li>
            <li><a href="/pos_overview.php">POs</a></li>
        </ul>
    </div>
</div>
<div class="row-fluid">
    <div class="span6 text-left">
        <select>
            <option>Job Name</option>
        </select>
    </div>
    <div class="span6 text-right">
        <span class="delete_job"><i class="icon-pencil"></i> Edit Budget</span>
    </div>
</div>

<div class="row-fluid">
    <div style="max-height: 500px; overflow-y: auto">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Cost Code</th>
                    <th>Estimate</th>
                    <th>POs</th>
                    <th>Variance</th>
                    <th>Total PO Cost</th>
                    <th>+/- Estimate</th>
                    <th>Paid</th>
                    <th>Outstanding</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="2" class="text-right">
                        TOTAL: 
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td class="child_rows_open" data-row="1">
                        <i class="icon-plus-sign child_rows"></i>
                        <i class="icon-minus-sign child_rows" style="display:none"></i>
                    </td>
                    <td colspan="8">Some Stuff</td>
                </tr>
                <tr data-row="row_1" style="display:none">
                    <td></td>
                    <td>Stuff</td>
                    <td>Stuff</td>
                    <td>Stuff</td>
                    <td>Stuff</td>
                    <td>Stuff</td>
                    <td>Stuff</td>
                    <td>Stuff</td>
                    <td>Stuff</td>
                </tr>
                <tr data-row="row_1" style="display:none">
                    <td></td>
                    <td>Stuff1</td>
                    <td>Stuff1</td>
                    <td>Stuff1</td>
                    <td>Stuff1</td>
                    <td>Stuff1</td>
                    <td>Stuff1</td>
                    <td>Stuff1</td>
                    <td>Stuff1</td>
                </tr>
            </tbody>

        </table>
    </div>
</div>


<? include 'layout/footer.php'; ?>
