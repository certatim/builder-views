<? include 'layout/header.php'; ?>

<div class="row-fluid">
    <div class="span12 text-left">
        <a href="#">Return to PO Overview</a>
    </div>
</div>
<form>
<? include 'po_header.php'; ?>

    <div class="row-fluid">
        <div style="max-height: 500px; overflow-y: auto">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Cost Code</th>
                        <th>Name</th>
                        <th>Unit Cost</th>
                        <th>Quantity</th>
                        <th>Metric</th>
                        <th>Total($)</th>
                        <th>Comments</th>
                        <th>Attachments</th>
                        <th>Complete</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="add_po_row" data-row="1">
                            <i class="icon-plus"></i>
                        </td>
                        <td colspan="9"></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</form>


<? include 'layout/footer.php'; ?>
