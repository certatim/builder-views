<? include 'layout/header.php'; ?>

<h3>Dashboard</h3>
<a href="/dashboard.php">Dashboard</a> <br />

<h3>Jobs</h3>
<a href="/new_job.php">New Job</a> <br />

<a href="/job_dashboard.php">Job Dashboard</a> <br />

<h3>Purchase Orders</h3>
<a href="/pos_overview.php">PO's overview</a> <br />

<a href="/budget.php">Budget</a> <br />

<a href="/new_po.php">New PO</a> <br />

<a href="/payment_agreement.php">Payment Agreement</a> <br />

<a href="/outstanding_pos.php">Outstanding PO's</a> <br />

<h3>Bids</h3>
<a href="/bid_overview.php">Bid Overview</a> <br />

<a href="/bid_submission.php">Bid Submission</a> <br />

<h3>Budget</h3>
<a href="/budget.php">Budget Overview</a> <br />

<a href="/budget_create.php">Budget Create</a> <br />


<h3>Code Configuration</h3>
<a href="/cost_code_overview.php">Cost Code Overview</a> <br />

<a href="/subcontractors.php">Subcontractors</a> <br />

<a href="/new_subcontractor.php">New subcontractor</a> <br />
    
<? include 'layout/footer.php'; ?>