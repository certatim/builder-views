<? include 'layout/header.php'; ?>
<form class="mega-form">
    <div class="row-fluid form_row">
        <h3>JOB INFORMATON</h3>
        <div class="span6 form-horizontal">

            <div class="control-group">
                <label class="control-label" for="job_group">Job Group: </label>
                <div class="controls">
                    <div class="add_value_select">
                        <select id="job_group"name=""  >
                            <option>Combo Box</option>
                        </select>
                        <i class="icon-plus" data-add_value="job_group"></i>
                    </div>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="job_name">Job Name: </label>
                <div class="controls">
                    <input type="text" name="" id="job_name" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="address">Address: </label>
                <div class="controls">
                    <input type="text" name="" id="address" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="city">City: </label>
                <div class="controls">
                    <input type="text" name="" id="city" class="input-prepend input-small" />
                    <label for="state">State:</label>
                    <input type="text" name="" id="state" class="input-mini" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="zip">ZIP</label>
                <div class="controls">
                    <input type="text" name="" id="zip" class="input-small" />
                </div>
            </div>
        </div>



        <div class="span6 form-horizontal">

            <div class="control-group">
                <label class="control-label" for="lot_info">Lot Info: </label>
                <div class="controls">
                    <input type="text" name="" id="lot_info" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="permit_number">Permit #: </label>
                <div class="controls">
                    <input type="text" name="" id="permit_number" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="contact_price">Contract Price: </label>
                <div class="controls">
                    <input type="text" name="" id="contact_price" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="project_manager">Project Manager: </label>
                <div class="controls">
                    <select id="project_manager"name=""  >
                        <option>Combo Box</option>
                    </select>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="job_prefix">Job Prefix</label>
                <div class="controls">
                    <input type="text" name="" id="job_prefix" />
                </div>
            </div>
        </div>
    </div>


    <div class="row-fluid form_row">
        <h3>JOB SCHEDULE</h3>
        <div class="span6 form-horizontal">

            <div class="control-group">
                <label class="control-label" for="project_start">Project Start: </label>
                <div class="controls input-append date" id="project_start" data-date-format="dd-mm-yyyy" style="margin-left:20px">
                    <input type="text" name="" />
                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>


            <div class="control-group">
                <label class="control-label" for="project_start">Project Completion: </label>
                <div class="controls input-append date" id="project_completion" data-date-format="dd-mm-yyyy" style="margin-left:20px">
                    <input type="text" name="" />
                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label">Work Days: </label>
                <div class="controls days">
                    <div>
                        <label class="" for="sunday">Sun</label><br />
                        <input type="checkbox" name="" id="sunday"/>
                    </div>
                    <div>
                        <label class="" for="monday">Mon</label><br />
                        <input type="checkbox" name="" id="monday" />
                    </div>
                    <div>                
                        <label class="" for="tuesday">Tue</label><br />
                        <input type="checkbox" name="" id="tuesday" />
                    </div>
                    <div>                
                        <label class="" for="wednesday">Wed</label><br />
                        <input type="checkbox" name="" id="wednesday" />
                    </div>
                    <div>                
                        <label class="" for="thursday">Thu</label><br />
                        <input type="checkbox" name="" id="thursday" />
                    </div>
                    <div>                
                        <label class="" for="friday">Fri</label><br />
                        <input type="checkbox" name="" id="friday" />
                    </div>
                    <div>                
                        <label class="" for="saturday">Sat</label><br />
                        <input type="checkbox" name="" id="saturday" />
                    </div>
                </div>
            </div>
        </div>
        <div class="span6 form-horizontal">

            <div class="control-group">
                <label class="control-label" for="actual_start">Actual Start: </label>
                <div class="controls input-append date" id="actual_start" data-date-format="dd-mm-yyyy" style="margin-left:20px">
                    <input type="text" name="" />
                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="actual_completion">Actual Completion: </label>
                <div class="controls input-append date" id="actual_completion" data-date-format="dd-mm-yyyy" style="margin-left:20px">
                    <input type="text" name="" />
                    <span class="add-on"><i class="icon-calendar"></i></span>
                </div>
            </div>

        </div>

    </div>


    <div class="row-fluid form_row">
        <h3>CONTACT INFORMATION</h3>
        <div class="span6 form-horizontal">

            <h4>Owner Information</h4>

            <div class="control-group">
                <label class="control-label" for="owner_name">Name: </label>
                <div class="controls">
                    <input type="text" name="" id="owner_name" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="owner_address">Address: </label>
                <div class="controls">
                    <input type="text" name="" id="owner_address" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="owner_city">City: </label>
                <div class="controls">
                    <input type="text" name="" id="owner_city" class="input-prepend input-small" style="/*width:120px*/"/>
                    <label for="owner_state">State:</label>
                    <input type="text" name="" id="owner_state" class="input-mini" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="owner_zip">ZIP</label>
                <div class="controls">
                    <input type="text" name="" id="owner_zip" class="input-small" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="owner_phone">Home Phone: </label>
                <div class="controls">
                    <input type="text" name="" id="owner_phone" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="owner_cell">Cell Phone: </label>
                <div class="controls">
                    <input type="text" name="" id="owner_cell" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="owner_email">Email: </label>
                <div class="controls">
                    <input type="text" name="" id="owner_email" />
                </div>
            </div>
        </div>



        <div class="span6 form-horizontal">

            <h4>Point Of Contact</h4>
            <div class="copy_info">
                <input type="checkbox" data-input_original="owner_name,owner_phone,owner_email" data-input_destination="contact_name,contact_phone,contact_email">
                <span>Same as Owner</span>
            </div>


            <div class="control-group">
                <label class="control-label" for="contact_name">Name: </label>
                <div class="controls">
                    <input type="text" name="" id="contact_name" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="contact_phone">Phone: </label>
                <div class="controls">
                    <input type="text" name="" id="contact_phone" />
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="contact_email">Email: </label>
                <div class="controls">
                    <input type="text" name="" id="contact_email" />
                </div>
            </div>
            <button class="btn btn-success" id="submit_job">Save and Continue</button>
            <button class="btn btn-danger" id="cancel_job">Cancel</button>
        </div>
    </div>







</form>

<? include 'layout/footer.php'; ?>