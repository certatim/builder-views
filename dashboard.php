<? include 'layout/header.php'; ?>

<div class="row-fluid">
    <div class="span4">
        <h3>Pending Tasks</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Expected</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <a href="#">See More...</a>
    </div>
    <div class="span4">
        <h3>Outstanding PO's</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Job</th>
                    <th>PO#</th>
                    <th>Outstanding Amount</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <a href="#">See More...</a>
    </div>
    <div class="span4">
        <h3>Outstanding Bids</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Job</th>
                    <th>Material</th>
                    <th>Vendor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <a href="#">See More...</a>
    </div>
</div>

<div class="row-fluid">
    <div class="span4">
        <h3>Unreceived Material</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Expected</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
        <a href="#">See More...</a>
    </div>
    <div class="span8">
        <h3>Recent Activity</h3>
        <div id="recent_activities">
            <div class="recent_activity well">
                <span>PO added to JobName</span>
                <span class="recent_activity_date">Tue, Jun 22nd 2013</span>
            </div>
        </div>
        <a href="#">See More...</a>
    </div>
</div>


<? include 'layout/footer.php'; ?>
