<div class="row-fluid">
    <div class="span6 form-horizontal">
        <div class="control-group">
            <label class="control-label" for="job">Job: </label>
            <div class="controls">
                <select id="job" name=""  >
                    <option>Job Name</option>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="po_serial">Purchase Order#: </label>
            <div class="controls">
                <!-- if(isset($po_id) && $po_id): -->
                    <input class="input-mini" type="text" name="" id="po_serial" /> - <input class="input-small" type="text" name="" id="po_number" />
                <!-- else: -->
<!--                    <select>
                        <option>Purchase order Name</option>
                    </select>
-->
                <!-- endif; -->
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="job_title">Job Title: </label>
            <div class="controls">
                <input type="text" name="" id="job_title" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="job_start_date">Anticipated Start Date: </label>
            <div class="controls">
                <input type="text" name="" id="job_start_date" />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="job_expected_completion">Expected Completion: </label>
            <div class="controls">
                <input type="text" name="" id="job_expected_completion" />
                <div class="supplementary_checkbox">
                    <input type="checkbox" name="" id="dont_schedule_po" />
                    <label class="" for="dont_schedule_po">Don't schedule PO</label>
                </div>
            </div>
        </div>
    </div>
    <div class="span6 form-horizontal">
        <div class="control-group">
            <label class="control-label" for="subcontractor">Subcontractor: </label>
            <div class="controls">
                <select id="subcontractor" name=""  >
                    <option>Select Subcontractor</option>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="po_type">Purchase Order Type: </label>
            <div class="controls">
                <select id="po_type" name=""  >
                    <option>Select Type</option>
                </select>
                <div class="supplementary_checkbox">
                    <input type="checkbox" name="" id="includes_material" disabled/>
                    <label class="inactive" for="includes_material">Includes Material</label>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="job_title">Contract: </label>
            <!-- Here will be some weird jquery stuff combined with graphics and hidden input defining if there is a contract or not -->
        </div>

        <div class="control-group">
            <label class="control-label" for="job_start_date">Date Created: </label>
            <div class="controls">
                <span>11/10/2013</span>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="job_expected_completion">Status: </label>
            <div class="controls">
                <!-- images and php checking variables -->
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs">
            <li><a href="/new_po.php">Details</a></li>
            <li><a href="/payment_agreement.php">Payment Agreement</a></li>
            <li><a href="/payment_log.php">Payment Log</a></li>
        </ul>
    </div>
</div>
<div class="row-fluid">
    <div class="span6 text-left">
        <h2>Total: </h2>
    </div>
    <div class="span6 text-right">
        <h2>Outstanding Balance: </h2>
    </div>
</div>
