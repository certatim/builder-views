<?php

function config_item($string)
{
    return '/';
}
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=10" />
        <meta name="format-detection" content="telephone=no" />
        <title><? //= $title_for_layout;       ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=0.8">
        <link rel="stylesheet" type="text/css" href="<?= config_item('base_url_sub'); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="<?= config_item('base_url_sub'); ?>assets/css/bootstrap-responsive.min.css">
        <link rel="stylesheet" type="text/css" href="<?= config_item('base_url_sub'); ?>assets/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" type="text/css" href="<?= config_item('base_url_sub'); ?>assets/css/site.css">

        <!-- Styles --->
        <? //= $css_for_layout  ?>
        <!------------->

        <script type="text/javascript" src="<?= config_item('base_url_sub'); ?>assets/js/jquery.js"></script>
        <script type="text/javascript" src="<?= config_item('base_url_sub'); ?>assets/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= config_item('base_url_sub'); ?>assets/js/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="<?= config_item('base_url_sub'); ?>assets/js/jquery.tablesorter.min.js"></script>


    </head>
    <body>

        <div id="header" class="row-fluid">
            <div id="logo" class="span3">
                <a href="/">
                    <img src="<?= config_item('base_url_sub'); ?>assets/img/logo.png" />
                </a>
            </div>
            <div id="dropdown_menu" class="span6">
                <select id="jobs">
                    <option>Jobs</option>
                    <option>Erin Knight</option>
                    <option>Zero Nine</option>
                </select>
                <select id="create_new">
                    <option>Create New</option>
                    <option>Job</option>
                    <option>Purchase Order</option>
                    <option>Bid</option>
                </select>
            </div>
            <div id="control_bar" class="span3">
                <a href="#">Config</a> | <a href="#">Logout</a>
            </div>
        </div>
        <div id="wrapper">
