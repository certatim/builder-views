<? include 'layout/header.php'; ?>

<div class="row-fluid">
    <div class="span10">
        <ul class="nav nav-tabs">
            <li><a href="/cost_code_overview.php">Cost Codes</a></li>
            <li><a href="#">Variance Code</a></li>
            <li><a href="/subcontractors.php">Subcontractor</a></li>
            <li><a href="/material.php">Material</a></li>
            <li><a href="/material.php">Labor</a></li>
        </ul>
    </div>
    <div class="span2">
        <select>
            <option>Create New</option>
        </select>
    </div>
</div>
<div class="row-fluid">
    <div class="span8 cost_codes">
        <ul>
            <li>
                <div><i class="icon-chevron-down"></i>Level1-1</div>
                <ul>
                    <li>
                        <div><i class="icon-chevron-down"></i>Level2-1</div>
                        <ul>
                            <li>
                                <div><i class="icon-chevron-down"></i>Level3-1</div>
                                <ul>
                                    <li>
                                        <div>Level4-1<span class="delete_cc" data-name="Level4-1" data-id="4">X</span></div>
                                    </li>
                                    <li>
                                        <div>Level4-2<span class="delete_cc" data-name="Level4-2" data-id="5">X</span></div>
                                    </li>
                                    <li>
                                        <div>Level4-3<span class="delete_cc" data-name="Level4-3" data-id="6">X</span></div>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <div><i class="icon-chevron-down"></i>Level3-2</div>
                                <ul>
                                    <li>
                                        <div>Level4-1<span class="delete_cc">X</span></div>
                                    </li>
                                    <li>
                                        <div>Level4-2<span class="delete_cc">X</span></div>
                                    </li>
                                    <li>
                                        <div>Level4-3<span class="delete_cc">X</span></div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>



<? include 'layout/footer.php'; ?>