<? include 'layout/header.php'; ?>

<h3>Outstanding Bids</h3>
<div id="jobs_select">
    <select>
        <option>All Jobs</option>
    </select>
</div>
<div class="row-fluid">
    <div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>JOB</th>
                    <th>MATERIAL</th>
                    <th>VENDOR</th>
                    <th>NOTES</th>
                    <th>AMOUNT</th>
                    <th>ATTACHMENTS</th>
                    <th>STATUS</th>
                    <th>ACTION</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>  
                </tr>
                <tr>
                    <td id="new_bid">
                        <i class="icon-plus"></i>
                    </td>
                    <td colspan="7"></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>




<? include 'layout/footer.php'; ?>