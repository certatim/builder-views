<? include 'layout/header.php'; ?>
<h1 class="text-center">Dashboard - Job Title</h1>
<div class="row-fluid">
    <div class="span6 text-center">
        <div class="dashboard">
            <h2>Summary</h2>
            <div class="dashboard_block">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2">
                                Contact Information
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Owner:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Person of contact:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Project Manager:</td>
                            <td>
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>

            <div class="dashboard_block">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="4">
                                Recent PO Activity
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>

                </table>
            </div>

            <div class="dashboard_block">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2">
                                Budget Summary
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Contract Price:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Approved Orders:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Project Running Total:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Total Direct Cost:</td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>Gross Profit:</td>
                            <td>
                            </td>
                        </tr>
                    </tbody>

                </table>
            </div>
        </div>
    </div>



    <div class="span6 text-center">
        <div class="dashboard">
            <h2>Calendar</h2>
            <div class="dashboard_block">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="3">
                                Reminders
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>

                </table>
            </div>

            <div class="dashboard_block">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="1">
                                Today's Schedule
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                        </tr>
                    </tbody>

                </table>
            </div>

            <div class="dashboard_block">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="2">
                                Daily Log
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>




</div>

<? include 'layout/footer.php'; ?>
