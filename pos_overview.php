<? include 'layout/header.php'; ?>

<div class="row-fluid">
    <div class="span6 text-left">
        <h2>Job Name</h2>
    </div>
    <div class="span6 text-right">
        <p>Total Budget: $</p>
        <p>Spend-To-Date: $</p>
        <p>Outstanding PO's: $</p>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <ul class="nav nav-tabs">
            <li><a href="/budget.php">Budget</a></li>
            <li><a href="/pos_overview.php">POs</a></li>
        </ul>
    </div>
</div>
<div class="row-fluid">
    <div class="span6 text-left">
        <select>
            <option>Job Name</option>
        </select>
    </div>
    <div class="span6 text-right">
        <span class="delete_job"><i class="icon-trash"></i></span>
    </div>
</div>

<div class="row-fluid">
    <div style="max-height: 500px; overflow-y: auto">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>PO#</th>
                    <th>Title</th>
                    <th>Cost Code</th>
                    <th>Files</th>
                    <th>POConfirmed</th>
                    <th>Completed</th>
                    <th>Paid</th>
                    <th>Cost</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="9" class="text-right">
                        TOTAL LISTED: 
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


<? include 'layout/footer.php'; ?>